import template from './app.html';
import './app.less';
import 'sem-ui/reset.css';
import 'sem-ui/site.css';
import 'sem-ui/container.css';

let appComponent = {
  template,
  restrict: 'E'
};

export default appComponent;
