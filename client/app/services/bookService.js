import booksObject from '../store/booksById';
import top10BookIds from '../store/top10BookIds';
import top10AuthorIds from '../store/top10AuthorIds';

const bookServiceModule = angular.module('bookService', [])

.factory('getBooks', () => {
  return function () {
    return Object.keys(booksObject).map(id => ({
      id,
      title: booksObject[id].title,
      author_fl: booksObject[id].author_fl,
      rating: booksObject[id].rating,
      cover: booksObject[id].cover
    }));
  };
})

.factory('getBookById', () => {
  return function (id) {
    return booksObject[id] || null;
  };
})

.factory('getTop10Books', () => {
  return function () {
    return top10BookIds.map(id => ({
      id,
      value: booksObject[id].title
    }));
  };
})

.factory('getTop10Authors', () => {
  return function () {
    return top10AuthorIds.map(id => ({
      id,
      value: booksObject[id].author_fl
    }));
  };
})

.name;

export default bookServiceModule;
