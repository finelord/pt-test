import angular from 'angular';
import bookService from './bookService';

let servicesModule = angular.module('app.services', [
  bookService
])

.name;

export default servicesModule;
