import angular from 'angular';
import Top from './top/top';
import Book from './book/book';
import Search from './search/search';
import Navbar from './navbar/navbar';
import List from './list/list';

let componentsModule = angular.module('app.components', [
  Top,
  Book,
  Search,
  Navbar,
  List
])

.name;

export default componentsModule;
