class SearchController {
  constructor($scope, $location, getBooks) {
    $scope.sortorder = '-rating';
    this.name = 'search';
    this.books = getBooks();
    this.viewBookDetails = bookId => $location.path(`/book/${bookId}`);
    this.search = function(item) {
      const q = ($scope.query || '').toLowerCase();
      if (!$scope.query || (q.length >= 2 && (
        ((item.title || '').toLowerCase().indexOf(q) != -1) ||
        ((item.author_fl || '').toLowerCase().indexOf(q) != -1)
      ))) {
        return true;
      }
      return false;
    };
  }
}

SearchController.$inject = ['$scope', '$location', 'getBooks'];

export default SearchController;
