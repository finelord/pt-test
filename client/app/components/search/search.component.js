import template from './search.html';
import controller from './search.controller';
import './search.less';
import 'sem-ui/grid.css';
import 'sem-ui/icon.css';
import 'sem-ui/input.css';
import 'sem-ui/item.css';
import 'sem-ui/list.css';
import 'sem-ui/image.css';

let searchComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default searchComponent;
