import SearchModule from './search'
import Services from '../../services';

describe('Search', () => {
  let $rootScope, $state, $location, $componentController, $compile;

  beforeEach(window.module(SearchModule));
  beforeEach(window.module(Services));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $componentController = $injector.get('$componentController');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
    it('default component should be search', () => {
      $location.url('/search');
      $rootScope.$digest();
      expect($state.current.component).to.eq('search');
    });
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = $componentController('search', {
        $scope: $rootScope.$new()
      });
    });

    it('has a name property', () => {
      expect(controller).to.have.property('name');
    });
  });

  describe('View', () => {
    // view layer specs
    let scope, template;

    beforeEach(() => {
      scope = $rootScope.$new();
      template = $compile('<search></search>')(scope);
      scope.$apply();
    });

    it('has required amount of controls', () => {
      expect(template.find('input')).to.have.lengthOf(5);
    });

  });
});
