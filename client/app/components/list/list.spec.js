import ListModule from './list'

describe('List', () => {
  let $rootScope, $componentController, $compile;

  beforeEach(window.module(ListModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $componentController = $injector.get('$componentController');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = $componentController('list', {
        $scope: $rootScope.$new()
      });
    });

    it('has a name property', () => {
      expect(controller).to.have.property('name');
    });
  });

  describe('View', () => {
    // view layer specs
    let scope, template;
    const data = [
      {id: 1, value: 'x'},
      {id: 2, value: 'y'},
      {id: 3, value: 'z'}
    ]

    beforeEach(() => {
      scope = $rootScope.$new();
      template = $compile('<list data="data"></list>')(scope);
      scope.$apply();
    });

    it('has expected amount of items', () => {
      scope.data = data;
      scope.$apply();
      expect(template.find('list-item')).to.have.lengthOf(3);
    });

  });
});
