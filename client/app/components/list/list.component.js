import template from './list.html';
import controller from './list.controller';
import './list.less';
import 'sem-ui/list.css'

let listComponent = {
  restrict: 'E',
  bindings: {
    data: '<',
    onItemClick: '&'
  },
  template,
  controller
};

export default listComponent;
