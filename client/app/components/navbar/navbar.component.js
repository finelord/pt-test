import template from './navbar.html';
import controller from './navbar.controller';
import './navbar.less';
import 'sem-ui/menu.css';

let navbarComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default navbarComponent;
