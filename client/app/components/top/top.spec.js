import TopModule from './top';
import Services from '../../services';

describe('Top', () => {
  let $rootScope, $state, $location, $componentController, $compile;

  beforeEach(window.module(TopModule));
  beforeEach(window.module(Services));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $componentController = $injector.get('$componentController');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
    it('default component should be top', () => {
      $location.url('/');
      $rootScope.$digest();
      expect($state.current.component).to.eq('top');
    });
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = $componentController('top', {
        $scope: $rootScope.$new()
      });
    });

    it('has a name property', () => {
      expect(controller).to.have.property('name');
    });
  });

  describe('View', () => {
    // view layer specs
    let scope, template;

    beforeEach(() => {
      scope = $rootScope.$new();
      template = $compile('<top></top>')(scope);
      scope.$apply();
    });

    it('has 2 titles', () => {
      expect(template.find('h2')).to.have.lengthOf(2);
    });

  });
});
