import template from './top.html';
import controller from './top.controller';
import './top.less';
import 'sem-ui/header.css'

let topComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default topComponent;
