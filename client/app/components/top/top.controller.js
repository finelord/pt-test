class TopController {
  constructor($location, getTop10Books, getTop10Authors) {
    this.name = 'top';
    this.top10Books = getTop10Books();
    this.top10Authors = getTop10Authors();
    this.viewBookDetails = bookId => $location.path(`/book/${bookId}`);
  }
}

TopController.$inject = ['$location', 'getTop10Books', 'getTop10Authors'];

export default TopController;
