import angular from 'angular';
import uiRouter from 'angular-ui-router';
import topComponent from './top.component';

let topModule = angular.module('top', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('top', {
      url: '/',
      component: 'top'
    });
})

.component('top', topComponent)

.name;

export default topModule;
