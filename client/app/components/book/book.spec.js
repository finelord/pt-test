import BookModule from './book'
import Services from '../../services';

describe('Book', () => {
  let $rootScope, $state, $location, $componentController, $compile;

  beforeEach(window.module(BookModule));
  beforeEach(window.module(Services));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $componentController = $injector.get('$componentController');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
    it('default component should be book', () => {
      $location.url('/book/fakeid');
      $rootScope.$digest();
      expect($state.current.component).to.eq('book');
    });
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = $componentController('book', {
        $scope: $rootScope.$new()
      });
    });

    it('has a name property', () => {
      expect(controller).to.have.property('name');
    });
  });

  describe('View', () => {
    // view layer specs
    let scope, template;
    const book = {
      'title': 'Masterpiece',
      'author_fl': 'Genius'
    }

    beforeEach(() => {
      scope = $rootScope.$new();
      template = $compile('<book book="book"></book>')(scope);
      scope.$apply();
    });

    it('has proper book title and author', () => {
      scope.book = book;
      scope.$apply();
      expect(template.find('h1').html()).to.contain(`${book.title}`);
      expect(template.find('author').html()).to.contain(`by ${book.author_fl}`);
    });

  });
});
