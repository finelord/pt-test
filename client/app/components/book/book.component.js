import template from './book.html';
import controller from './book.controller';
import './book.less';
import 'sem-ui/item.css';
import 'sem-ui/image.css';

let bookComponent = {
  restrict: 'E',
  bindings: {
    book: '<'
  },
  template,
  controller
};

export default bookComponent;
