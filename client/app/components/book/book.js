import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookComponent from './book.component';

let bookModule = angular.module('book', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('book', {
      url: '/book/{bookId}',
      component: 'book',
      resolve: {
        book: function ($transition$, getBookById) {
          const id = $transition$.params().bookId;
          return getBookById(id);
        }
      }
    });
})

.component('book', bookComponent)

.name;

export default bookModule;
