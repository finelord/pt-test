import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from './services';
import Components from './components';
import AppComponent from './app.component';

angular.module('app', [
  uiRouter,
  Services,
  Components
])

.config(($locationProvider) => {
  "ngInject";
  $locationProvider.html5Mode(true).hashPrefix('!');
})

.component('app', AppComponent);
