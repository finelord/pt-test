import <%= upCaseName %>Module from './<%= name %>'

describe('<%= upCaseName %>', () => {
  let $rootScope, $componentController, $compile;

  beforeEach(window.module(<%= upCaseName %>Module));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $componentController = $injector.get('$componentController');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    let controller;
    beforeEach(() => {
      controller = $componentController('<%= name %>', {
        $scope: $rootScope.$new()
      });
    });

    it('has a name property', () => {
      expect(controller).to.have.property('name');
    });
  });

  describe('View', () => {
    // view layer specs
    let scope, template;

    beforeEach(() => {
      scope = $rootScope.$new();
      template = $compile('<<%= name %>></<%= name %>>')(scope);
      scope.$apply();
    });

    it('has name in template', () => {
      expect(template.find('h1').html()).to.eq('<%= name %>');
    });

  });
});
