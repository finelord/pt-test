# README #

Bookshelf app

### How do I get set up? ###

* `npm i` to install all required dependencies (skip Semantic-UI post-install by `CRTL+C`)
* `gulp serve` to start development
* `npm test` to run tests
* `gulp webpack` to build prod bundle
* `gulp component --name=%component-name%` to generate new component